#!/usr/bin/perl
use 5.010;
use strict;
use warnings;
use Scalar::Util qw(
    blessed
);
use Try::Tiny;
use Kafka::Connection;
use Kafka::Producer;
my ( $connection, $producer );
try {
    #-- Connection
    $connection = Kafka::Connection->new( host => '192.168.2.201' );
    #-- Producer
    $producer = Kafka::Producer->new( Connection => $connection );
    # Sending a single message
    my $bin = '/root/example/practice/testf';
    while (1) {
        # body...
        my $res = `$bin`;
        my $response = $producer->send(
            'mytopic',
            0,
          $res # message
    );
    }
} catch {
    my $error = $_;
    if ( blessed( $error ) && $error->isa( 'Kafka::Exception' ) ) {
        warn 'Error: (', $error->code, ') ',  $error->message, "\n";
        exit;
    } else {
        die $error;
    }
};
# Closes the producer and cleans up
undef $producer;
$connection->close;
undef $connection;