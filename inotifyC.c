#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <unistd.h>


#define MAX_EVENT_MONITOR 2048
#define NAME_LEN 32
#define MONITOR_EVENT_SIZE (sizeof(struct inotify_event))
#define BUFFER_LEN MAX_EVENT_MONITOR * (MONITOR_EVENT_SIZE + NAME_LEN)

int main(int argc, char** argv){

        int fd, watch_desc;
        char buffer[BUFFER_LEN];

        fd = inotify_init();
        if(fd<0){
                perror("Notify not Initialize");
        }

        watch_desc = inotify_add_watch(fd,"/root/example/",IN_CREATE);

 /*     if(watch_desc == -1 )
        {
                printf("Couldn't add watch to  %s\n", "/root");
        }
        else {
                printf("Monitor added to        path    %s\n", "/root");
        }
*/
        int i = 0;
        while (1){
                i = 0;
                int total_read = read(fd, buffer, BUFFER_LEN);
                if(total_read<0){
                        perror("read error");
                }
                while(i<total_read){
                        struct inotify_event *event = (struct inotify_event*)&buffer[i];
                        if(event->len){
                                if(event->mask & IN_CREATE){
                                        if(event->mask & IN_ISDIR){
                                                printf("Directory \"%s\" was created\n",event->name);
                                                return 0;
                                        }
                                        else{
                                                 printf("File \"%s\" was created\n", event->name);
                                                return 0;
                                        }
                                }
                                i += MONITOR_EVENT_SIZE+event->len;
                        }
                }
        }
        inotify_rm_watch(fd, watch_desc);
        close(fd);
        return (EXIT_SUCCESS);
}
